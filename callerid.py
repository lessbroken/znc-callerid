'''
callerid.py, a ZNC module to remember who you've /ACCEPTed on freenode

contact: empty_string @ freenode

Usage of the works is permitted provided that this instrument is retained with
the works, so that any entity that uses the works is notified of this
instrument. DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
'''

import znc


class callerid(znc.Module):
    description = 'Save nicks allowed to bypass umode +g on freenode'
    module_types = [znc.CModInfo.NetworkModule]
    wiki_page = 'Callerid'
    has_args = True
    args_help_text = '"verbose" to enable verbose messages, blank otherwise'

    def OnLoad(self, args, message):
        self.verbose = False

        if 'verbose' in args:
            self.verbose = True

        self.command_state = 'normal'
        return True

    def HandleNormalCommand(self, command, args):
        if command == 'list':
            users = sorted(list(self.nv.keys()))
            if users:
                self.PutModule('Known users:')
                for user in users:
                    self.PutModule('  {}'.format(user))
            else:
                self.PutModule('No users.')

        elif command == 'clearall':
            self.PutModule('Are you sure? (yes/no)')
            self.command_state = 'confirm_clearall'

        else:
            self.PutModule('''
Callerid module help:
list        list users known to the module
clearall    clear the list of users
            '''.strip())

    def HandleConfirmClearAll(self, command, args):
        if command == 'yes':
            for user in self.nv.keys():
                del self.nv[user]
            self.PutModule('Deleted all users from list.')
        else:
            self.PutModule('Cancelled.')

        self.command_state = 'normal'

    def OnModCommand(self, command):
        args = command.split()
        command, args = args[0], args[1:]

        states = {'normal': self.HandleNormalCommand,
                  'confirm_clearall': self.HandleConfirmClearAll}

        states[self.command_state](command, args)

    def PutIfVerbose(self, message):
        if self.verbose:
            self.PutModule(message)

    def OnUserRaw(self, message):
        message = message.s.lower().split()

        if not (len(message) == 2 and message[0] == 'accept'):
            return znc.CONTINUE

        # At this point we know we're looking at a valid ACCEPT message.
        user = message[1]

        # "ACCEPT *" lets the server return a list of already-accepted names;
        # we don't want to handle this.
        if user == '*':
            return znc.CONTINUE

        # "ACCEPT -nick" will remove a nick from the accept list; we need to
        # catch this and remove it from our auto-accept list as well.
        adding = True
        if user[0] == '-':
            user = user[1:]
            adding = False

        # A comma-separated list of nicks is allowed.
        users = user.split(',')

        for user in users:
            if len(user) > 300:
                # Sanity check, no network should be allowing nicks this long
                # and it could cause us to go into a loop on connect.
                continue

            if adding:
                if user in self.nv:
                    self.PutIfVerbose('User {} already accepted.'.format(user))
                else:
                    # Store in module non-volatile storage; the 'yes' can be
                    # replaced with anything.
                    self.nv[user] = 'yes'
                    self.PutIfVerbose('Added {} to accept list.'.format(user))

            else:
                if user in self.nv:
                    del self.nv[user]
                    self.PutIfVerbose('Removed {} from accept list.'
                                      .format(user))
                else:
                    self.PutIfVerbose('User {} was never accepted.'
                                      .format(user))

        return znc.CONTINUE

    def OnIRCConnected(self):
        users = sorted(list(self.nv.keys()))
        self.PutIfVerbose('Auto-accepting {}.'.format(', '.join(users)))

        # We *can* accept multiple users at once, so we probably should.
        current_users = []
        while users or current_users:
            current_length = len(','.join(current_users))

            if users and current_length + len(users[0]) < 400:
                current_users.append(users.pop(0))
            else:
                self.PutIRC('ACCEPT {}'.format(','.join(current_users)))
                current_users.clear()
