# ZNC `callerid` module

The `callerid` module maintains a list of who you have `/ACCEPT`ed for networks
that support freenode-style "caller ID," in which you enable a user mode and
only PMs from previously whitelisted users are sent to you. Upon reconnection,
the module re-whitelists users you had previously whitelisted.

More information can be found on [the wiki page](https://wiki.znc.in/Callerid).

## Contact

You can contact me in `#znc` on freenode. I'm `empty_string`.
